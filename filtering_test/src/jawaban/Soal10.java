package jawaban;

import java.util.Arrays;
import java.util.Scanner;

public class Soal10 {

	public void jawaban() {
		Scanner scan = new Scanner(System.in);
		System.out.print("Input : ");
		String str = scan.nextLine();
		int len = str.length();
		
		
		str = str.toLowerCase();
		char[] arr = str.toCharArray();
		Arrays.sort(arr);
		String vokal = "";
		String konsonan ="";
		String others="";
		
		for(int i = 0 ; i < len ; i++) {
			
			if(arr[i] == 'a' || arr[i] == 'i' || arr[i] == 'u' || arr[i] == 'e' || arr[i] == 'o') {
				vokal += Character.toString(arr[i]) ;
				
			}
			else if(arr[i] > 'a' && arr[i] <='z') {
				konsonan += Character.toString(arr[i]) ;
				
			}
			else if(arr[i] != 32 ){
				others += Character.toString(arr[i]);
			}
		}
		System.out.println("Vokal :" + vokal);
		System.out.println("konsonan :" + konsonan);
		System.out.println("others :" + others);
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Soal10 test = new Soal10();
		test.jawaban();
		
		
	}

}
